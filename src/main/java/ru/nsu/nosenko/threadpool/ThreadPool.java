package ru.nsu.nosenko.threadpool;

import java.util.concurrent.*;

public class ThreadPool {
    private final ThreadPoolExecutor executorService;

    public ThreadPool(int nThreads) {
        this.executorService = new ThreadPoolExecutor(
                nThreads, nThreads,
                Long.MAX_VALUE, TimeUnit.DAYS,
                new LinkedBlockingQueue<Runnable>()
        );
    }

    public void perform(Runnable runnable) {
        executorService.execute(runnable);
    }

    public void shutdown() {
        executorService.shutdown();
    }

}

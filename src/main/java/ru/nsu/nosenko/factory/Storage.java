package ru.nsu.nosenko.factory;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Storage<T> {
    private final int capacity;
    private final Queue<T> queue;

    public Storage(int capacity){
        this.capacity = capacity;
        queue = new LinkedBlockingQueue<>(capacity);
    }

    public synchronized void put(T obj) {
        while (queue.size() >= capacity) // ложное срабатывание
            // ожидание освобождения места
            try {
                this.wait();
            } catch (InterruptedException e) { e.printStackTrace(); }
        queue.add(obj);
        //System.out.println("Склад " + this.getClass().getName() + " получил товар"); // лог
        this.notifyAll();  // хэй, пришла доставка, забирай
    }

    public synchronized T get() {
        while (queue.size() < 1)
            // ожидание на пустом складе
            try {
                this.wait();
            } catch (InterruptedException e) {e.printStackTrace(); }
        T obj = queue.remove();
       // System.out.println("Со склада ушел товар"); // лог
        this.notify(); // хэй, место освободилось, загружай
        return obj;
    }

    public synchronized int getSize(){
        return queue.size();
    }

    public synchronized int getCapacity(){
        return capacity;
    }

    public synchronized int getFullness(){ return (int)((float)getSize()/capacity*100); }
}

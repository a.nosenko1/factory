package ru.nsu.nosenko.factory.visual;

import ru.nsu.nosenko.factory.control.DynamicSettings;
import ru.nsu.nosenko.factory.control.VisualOperator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Window extends JFrame implements ActionListener, iVisual {
    private Duration BODY_DEALER_TIME;
    private Duration ENGINE_DEALER_TIME;

    private JProgressBar enginePr = new JProgressBar(0,100);
    private JProgressBar bodyPr = new JProgressBar(0,100);
    private JProgressBar accessoriesPr = new JProgressBar(0,100);
    private JProgressBar carPr = new JProgressBar(0,100);
    private JProgressBar queuePr = new JProgressBar(0,100);

    JLabel engine = new JLabel("engine");
    JLabel body = new JLabel("body");
    JLabel accessories = new JLabel("accessories");
    JLabel mainStorage = new JLabel("car storage");
    JLabel queue = new JLabel("queue");

    @Override
    public void setFullnessQueue(int percent) {
        queuePr.setValue(percent);
        String output = String.format("in the queue: %d", percent);
        queue.setText(output);
    }

    @Override
    public void setFullnessBodyStorage(int size, int capacity) {
        bodyPr.setValue((int)((float)size/capacity*100));
        String output = String.format("body %d/%d", size, capacity);
        body.setText(output);
    }

    @Override
    public void setFullnessEngineStorage(int size, int capacity) {
        enginePr.setValue((int)((float)size/capacity*100));
        String output = String.format("engine %d/%d", size, capacity);
        engine.setText(output);
    }

    @Override
    public void setFullnessAccessoriesStorage(int size, int capacity) {
        accessoriesPr.setValue((int)((float)size/capacity*100));
        String output = String.format("accessories %d/%d", size, capacity);
        accessories.setText(output);
    }

    @Override
    public void setFullnessCarStorage(int size, int capacity) {
        carPr.setValue((int)((float)size/capacity*100));
        String output = String.format("car storage %d/%d", size, capacity);
        mainStorage.setText(output);
    }

    private Duration ACCESSORIES_DEALER_TIME;
    private Duration CAR_DEALER_TIME;
    private Duration WORKER_TIME;
    private JLabel label;
    public Window(){
        super("factory");

        BODY_DEALER_TIME = Duration.ofSeconds(VisualOperator.getBODY_DEALER_TIME_DEFAULT());
        ENGINE_DEALER_TIME = Duration.ofSeconds(VisualOperator.getENGINE_DEALER_TIME_DEFAULT());
        ACCESSORIES_DEALER_TIME = Duration.ofSeconds(VisualOperator.getACCESSORIES_DEALER_TIME_DEFAULT());
        CAR_DEALER_TIME = Duration.ofSeconds(VisualOperator.getCAR_DEALER_TIME_DEFAULT());
        WORKER_TIME = Duration.ofSeconds(VisualOperator.getWORKER_TIME_DEFAULT());

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JSlider slider = new JSlider(0, 10);
        slider.setPaintLabels(true);
        slider.setMajorTickSpacing(1);

        JSlider slider2 = new JSlider(0, 10);
        slider2.setPaintLabels(true);
        slider2.setMajorTickSpacing(1);

        JSlider slider3 = new JSlider(0, 10);
        slider3.setPaintLabels(true);
        slider3.setMajorTickSpacing(1);

        JSlider slider4 = new JSlider(0, 30);
        slider4.setPaintLabels(true);
        slider4.setMajorTickSpacing(3);

        JSlider slider5 = new JSlider(0, 30);
        slider5.setPaintLabels(true);
        slider5.setMajorTickSpacing(3);

        slider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                ENGINE_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        slider2.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                BODY_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        slider3.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                ACCESSORIES_DEALER_TIME = Duration.ofSeconds(value);
            }
        });
        slider4.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                WORKER_TIME = Duration.ofSeconds(value);
            }
        });
        slider5.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider)e.getSource()).getValue();
                CAR_DEALER_TIME = Duration.ofSeconds(value);
            }
        });

        Font font = new Font("Verdana", Font.PLAIN, 14);

        engine.setFont(font);
        body.setFont(font);
        accessories.setFont(font);
        JLabel workers = new JLabel("workers");
        workers.setFont(font);
        JLabel dealers = new JLabel("dealers");
        dealers.setFont(font);

        JPanel contents = new JPanel(new VerticalLayout());

        enginePr.setStringPainted(true);
        carPr.setStringPainted(true);
        accessoriesPr.setStringPainted(true);
        bodyPr.setStringPainted(true);

        contents.add(engine);
        contents.add(enginePr);
        contents.add(slider);

        contents.add(body);
        contents.add(bodyPr);
        contents.add(slider2);

        contents.add(accessories);
        contents.add(accessoriesPr);
        contents.add(slider3);

        contents.add(workers);
        contents.add(slider4);

        contents.add(dealers);
        contents.add(slider5);

        mainStorage.setFont(font);
        contents.add(mainStorage);
        contents.add(carPr);

        queue.setFont(font);
        contents.add(queue);
        contents.add(queuePr);

        getContentPane().add(contents);

        setSize(250, 520);
        setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }

    @Override
    public Duration getBODY_DEALER_TIME() {
        return BODY_DEALER_TIME;
    }

    @Override
    public Duration getENGINE_DEALER_TIME() {
        return ENGINE_DEALER_TIME;
    }

    @Override
    public Duration getACCESSORIES_DEALER_TIME() {
        return ACCESSORIES_DEALER_TIME;
    }

    @Override
    public Duration getCAR_DEALER_TIME() {
        return CAR_DEALER_TIME;
    }

    @Override
    public Duration getWORKER_TIME() {
        return WORKER_TIME;
    }


}

class VerticalLayout implements LayoutManager
{
    private Dimension size = new Dimension();

    // Следующие два метода не используются
    public void addLayoutComponent   (String name, Component comp) {}
    public void removeLayoutComponent(Component comp) {}

    // Метод определения минимального размера для контейнера
    public Dimension minimumLayoutSize(Container c) {
        return calculateBestSize(c);
    }
    // Метод определения предпочтительного размера для контейнера
    public Dimension preferredLayoutSize(Container c) {
        return calculateBestSize(c);
    }
    // Метод расположения компонентов в контейнере
    public void layoutContainer(Container container)
    {
        // Список компонентов
        Component list[] = container.getComponents();
        int currentY = 5;
        for (int i = 0; i < list.length; i++) {
            // Определение предпочтительного размера компонента
            Dimension pref = list[i].getPreferredSize();
            // Размещение компонента на экране
            list[i].setBounds(5, currentY, pref.width, pref.height);
            // Учитываем промежуток в 5 пикселов
            currentY += 5;
            // Смещаем вертикальную позицию компонента
            currentY += pref.height;
        }
    }
    // Метод вычисления оптимального размера контейнера
    private Dimension calculateBestSize(Container c)
    {
        // Вычисление длины контейнера
        Component[] list = c.getComponents();
        int maxWidth = 0;
        for (int i = 0; i < list.length; i++) {
            int width = list[i].getWidth();
            // Поиск компонента с максимальной длиной
            if ( width > maxWidth )
                maxWidth = width;
        }
        // Размер контейнера в длину с учетом левого отступа
        size.width = maxWidth + 5;
        // Вычисление высоты контейнера
        int height = 0;
        for (int i = 0; i < list.length; i++) {
            height += 5;
            height += list[i].getHeight();
        }
        size.height = height;
        return size;
    }
}

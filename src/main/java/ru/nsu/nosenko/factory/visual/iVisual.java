package ru.nsu.nosenko.factory.visual;

import java.time.Duration;

public interface iVisual {
    public Duration getBODY_DEALER_TIME();
    public Duration getENGINE_DEALER_TIME();
    public Duration getACCESSORIES_DEALER_TIME();
    public Duration getCAR_DEALER_TIME();
    public Duration getWORKER_TIME();

    // по идее только оператор может это делать
    public void setFullnessBodyStorage(int size, int capacity);
    public void setFullnessEngineStorage(int size, int capacity);
    public void setFullnessAccessoriesStorage(int size, int capacity);
    public void setFullnessCarStorage(int size, int capacity);
    public void setFullnessQueue(int percent);
}

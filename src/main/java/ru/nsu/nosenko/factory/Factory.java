package ru.nsu.nosenko.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.nosenko.factory.control.DynamicSettings;
import ru.nsu.nosenko.factory.control.VisualOperator;
import ru.nsu.nosenko.factory.entities.Accessories;
import ru.nsu.nosenko.factory.entities.Body;
import ru.nsu.nosenko.factory.entities.Car;
import ru.nsu.nosenko.factory.entities.Engine;
import ru.nsu.nosenko.factory.runnables.*;
import ru.nsu.nosenko.factory.visual.Window;
import ru.nsu.nosenko.factory.visual.iVisual;
import ru.nsu.nosenko.threadpool.ThreadPool;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Factory {
    private Storage<Accessories> accessoriesStorage;
    private Storage<Body> bodyStorage;
    private Storage<Engine> engineStorage;
    private Storage<Car> carStorage;

    public static DynamicSettings dynamicSettings = new DynamicSettings();

    public static Logger logger = LoggerFactory.getLogger(Factory.class);

    public void main(String[] args) {
        iVisual Win = new Window();

        //logger.info("Example log from {} ", Factory.class.getSimpleName());

        FileInputStream fis = null;
        try {
            fis = new FileInputStream("src/main/resources/config.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Properties property = new Properties();
        try {
            property.load(fis);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        ThreadPool accessoriesThreadPool = new ThreadPool(Integer.parseInt(property.getProperty("AccessorySuppliers")));
        ThreadPool bodyThreadPool = new ThreadPool(1);
        ThreadPool engineThreadPool = new ThreadPool(1);
        ThreadPool workersThreadPool = new ThreadPool(Integer.parseInt(property.getProperty("Workers")));
        ThreadPool dealersThreadPool = new ThreadPool(Integer.parseInt(property.getProperty("Dealers")));
        ThreadPool controllersThreadPool = new ThreadPool(1);

        accessoriesStorage = new Storage<>(Integer.parseInt(property.getProperty("StorageAccessorySize")));
        bodyStorage = new Storage<>(Integer.parseInt(property.getProperty("StorageBodySize")));
        engineStorage = new Storage<>(Integer.parseInt(property.getProperty("StorageMotorSize")));
        carStorage = new Storage<>(Integer.parseInt(property.getProperty("StorageAutoSize")));

        Controller.Detector detector = new Controller.Detector();
        Queue<GetCar.Request> requestQueue = new LinkedBlockingQueue<>(Integer.parseInt(property.getProperty("RequestQueueSize")));

        VisualOperator.start(Win, dynamicSettings, engineStorage, bodyStorage, carStorage, accessoriesStorage, requestQueue);
        for (int i = 0; i < Integer.parseInt(property.getProperty("AccessorySuppliers")); i++) {
            accessoriesThreadPool.perform(new PutAccessories(accessoriesStorage));
        }
        bodyThreadPool.perform(new PutBody(bodyStorage));
        engineThreadPool.perform(new PutEngine(engineStorage));
        for (int i = 0; i < Integer.parseInt(property.getProperty("Workers")); i++) {
            workersThreadPool.perform(new Worker(accessoriesStorage, bodyStorage, engineStorage, carStorage, detector));
        }
        for (int i = 0; i < Integer.parseInt(property.getProperty("Dealers")); i++) {
            dealersThreadPool.perform(new GetCar(carStorage, requestQueue));
        }
        controllersThreadPool.perform(new Controller(carStorage, requestQueue, detector));

        accessoriesThreadPool.shutdown();
        engineThreadPool.shutdown();
        bodyThreadPool.shutdown();
        workersThreadPool.shutdown();
        dealersThreadPool.shutdown();
        controllersThreadPool.shutdown();
    }

}


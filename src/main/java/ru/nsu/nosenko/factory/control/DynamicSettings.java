package ru.nsu.nosenko.factory.control;

import java.time.Duration;

public class DynamicSettings {
    public static int BODY_DEALER_TIME_DEFAULT = 5;
    public static int ENGINE_DEALER_TIME_DEFAULT = 5;
    public static int ACCESSORIES_DEALER_TIME_DEFAULT = 5;
    public static int CAR_DEALER_TIME_DEFAULT = 15;
    public static int WORKER_TIME_DEFAULT = 15;
    public DynamicSettings(){
        BODY_DEALER_TIME        = Duration.ofSeconds(BODY_DEALER_TIME_DEFAULT);
        ENGINE_DEALER_TIME      = Duration.ofSeconds(ENGINE_DEALER_TIME_DEFAULT);
        ACCESSORIES_DEALER_TIME = Duration.ofSeconds(ACCESSORIES_DEALER_TIME_DEFAULT);
        CAR_DEALER_TIME         = Duration.ofSeconds(CAR_DEALER_TIME_DEFAULT);
        WORKER_TIME             = Duration.ofSeconds(WORKER_TIME_DEFAULT);
    }

    // скорость работы поставщиков - время ожидания перед итерацией
    private Duration BODY_DEALER_TIME;
    private Duration ENGINE_DEALER_TIME;
    private Duration ACCESSORIES_DEALER_TIME;
    private Duration CAR_DEALER_TIME;
    private Duration WORKER_TIME;

    protected void setBODY_DEALER_TIME(Duration BODY_DEALER_TIME){
        this.BODY_DEALER_TIME = BODY_DEALER_TIME;
    }
    protected void setENGINE_DEALER_TIME(Duration ENGINE_DEALER_TIME){
        this.ENGINE_DEALER_TIME = ENGINE_DEALER_TIME;
    }
    protected void setACCESSORIES_DEALER_TIME(Duration ACCESSORIES_DEALER_TIME){
        this.ACCESSORIES_DEALER_TIME = ACCESSORIES_DEALER_TIME;
    }
    protected void setCAR_DEALER_TIME(Duration CAR_DEALER_TIME){
        this.CAR_DEALER_TIME = CAR_DEALER_TIME;
    }
    protected void setWORKER_TIME(Duration WORKER_TIME){
        this.WORKER_TIME = WORKER_TIME;
    }

    public Duration BODY_DEALER_TIME        (){ return BODY_DEALER_TIME; }
    public Duration ENGINE_DEALER_TIME      (){ return ENGINE_DEALER_TIME; }
    public Duration ACCESSORIES_DEALER_TIME (){ return ACCESSORIES_DEALER_TIME; }
    public Duration CAR_DEALER_TIME         (){ return CAR_DEALER_TIME; }
    public Duration WORKER_TIME             (){ return WORKER_TIME; }
}


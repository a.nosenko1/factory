package ru.nsu.nosenko.factory.control;

import ru.nsu.nosenko.factory.Storage;
import ru.nsu.nosenko.factory.entities.Accessories;
import ru.nsu.nosenko.factory.entities.Body;
import ru.nsu.nosenko.factory.entities.Car;
import ru.nsu.nosenko.factory.entities.Engine;
import ru.nsu.nosenko.factory.runnables.GetCar;
import ru.nsu.nosenko.factory.visual.iVisual;

import java.time.Duration;
import java.util.Queue;

public class VisualOperator {
    // опрашивает визуальный интерфейс в отдельном потоке, настраивает DynamicSettings
    // а также дергает интерфейсы фабрики для отображения статусов
    static public void start(iVisual visual, DynamicSettings dynamicSettings,
                             final Storage<Engine> engineStorage, final Storage<Body> bodyStorage,
                             final Storage<Car> carStorage, final Storage<Accessories> accessoriesStorage,
                             final Queue<GetCar.Request> queue){
        Thread checker = new Thread(() -> {
            while (true){
                try {
                    Thread.sleep(Duration.ofNanos(500).toNanos());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                dynamicSettings.setACCESSORIES_DEALER_TIME(visual.getACCESSORIES_DEALER_TIME());
                dynamicSettings.setBODY_DEALER_TIME(visual.getBODY_DEALER_TIME());
                dynamicSettings.setWORKER_TIME(visual.getWORKER_TIME());
                dynamicSettings.setENGINE_DEALER_TIME(visual.getENGINE_DEALER_TIME());
                dynamicSettings.setCAR_DEALER_TIME(visual.getCAR_DEALER_TIME());

                visual.setFullnessEngineStorage(engineStorage.getFullness(), engineStorage.getCapacity());
                visual.setFullnessBodyStorage(bodyStorage.getSize(), bodyStorage.getCapacity());
                visual.setFullnessCarStorage(carStorage.getFullness(), carStorage.getCapacity());
                visual.setFullnessAccessoriesStorage(accessoriesStorage.getFullness(), accessoriesStorage.getCapacity());
                visual.setFullnessQueue(queue.size());
            }
        });
        checker.start();
    }

    public static int getBODY_DEALER_TIME_DEFAULT(){       return DynamicSettings.BODY_DEALER_TIME_DEFAULT;}
    public static int getENGINE_DEALER_TIME_DEFAULT(){     return DynamicSettings.ENGINE_DEALER_TIME_DEFAULT;}
    public static int getACCESSORIES_DEALER_TIME_DEFAULT(){return DynamicSettings.ACCESSORIES_DEALER_TIME_DEFAULT;}
    public static int getCAR_DEALER_TIME_DEFAULT(){        return DynamicSettings.CAR_DEALER_TIME_DEFAULT;}
    public static int getWORKER_TIME_DEFAULT(){            return DynamicSettings.WORKER_TIME_DEFAULT;}
}

package ru.nsu.nosenko.factory.runnables;

import ru.nsu.nosenko.factory.Factory;
import ru.nsu.nosenko.factory.Storage;
import ru.nsu.nosenko.factory.entities.Car;

import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

public class GetCar implements Runnable {
    // запросы дилеров попадают в очередь запросов
    // которую чекает контроллер
    private final Queue<Request> requestQueue;
    private final Storage<Car> carStorage;
    private final Stack<Request> buffer;
    private final int QUEUE_SIZE = 100;

    public GetCar(Storage<Car> carStorage, Queue<Request> requestQueue) {
        this.requestQueue = requestQueue;
        this.carStorage = carStorage;
        buffer = new Stack<>();
    }

    @Override
    public void run() {
        Runnable getCars = new Runnable() {
            @Override
            public void run() {
                while (true){
                    // можно wait
                    if (!buffer.isEmpty()){
                        Car car = carStorage.get();
                        Factory.logger.info("Получение: Dealer {}: Auto {} (Body: {} Motor: {} Accessory: {}) ",
                            Thread.currentThread().getId(), car.CID, car.body.BID, car.engine.EID, car.accessories.AID);
                        synchronized (requestQueue) {
                            requestQueue.remove();
                            requestQueue.notify();
                        }
                        buffer.pop();
                    }
                }
            }
        };
        Thread getter = new Thread(getCars);
        getter.start();
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.dynamicSettings.CAR_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (requestQueue) {
                Request request = new Request(666);
                while (requestQueue.size() >= QUEUE_SIZE){
                    try {
                        requestQueue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                requestQueue.add(request);
                buffer.push(request);
                requestQueue.notify();
            }

        }
    }

    static public class Request {
        public Request(int RID) {
            this.RID = RID;
        }

        private int RID;
    }
}

package ru.nsu.nosenko.factory.runnables;

import ru.nsu.nosenko.factory.Storage;
import ru.nsu.nosenko.factory.entities.Car;

import java.util.Queue;

public class Controller implements Runnable {
    // просыпается, когда со склада готовой продукции что-то отгрузили
    // анализирует запросы дилеров, смотрит на склад
    // если надо, запрашивает фабрику на создание новых изделий
    private final Queue<GetCar.Request> requestQueue;
    private final Storage<Car> carStorage;
    private final Detector detector;

    public Controller(Storage<Car> carStorage, Queue<GetCar.Request> requestQueue, Detector detector) {
        this.carStorage = carStorage;
        this.requestQueue = requestQueue;
        this.detector = detector;
    }

    @Override
    public void run() {
        while (true) {
            synchronized (requestQueue){
                while (requestQueue.size() < 1) {
                    try {
                        requestQueue.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                synchronized (detector){ detector.notify(); }
            }

        }

    }
    static public class Detector{};

}

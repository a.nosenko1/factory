package ru.nsu.nosenko.factory.runnables;

import ru.nsu.nosenko.factory.Factory;
import ru.nsu.nosenko.factory.Storage;
import ru.nsu.nosenko.factory.entities.Body;

import java.util.concurrent.TimeUnit;

public class PutBody implements Runnable {
    private final Storage<Body> bodyStorage;

    public PutBody(Storage<Body> bodyStorage) {
        this.bodyStorage = bodyStorage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.dynamicSettings.BODY_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //System.out.println("Кладу в склад кузовов");
            bodyStorage.put(new Body((int) (Math.random() * 1000)));
        }
    }
}

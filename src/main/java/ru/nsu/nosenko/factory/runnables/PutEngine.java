package ru.nsu.nosenko.factory.runnables;

import ru.nsu.nosenko.factory.Factory;
import ru.nsu.nosenko.factory.Storage;
import ru.nsu.nosenko.factory.entities.Engine;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

public class PutEngine implements Runnable {
    private final Storage<Engine> engineStorage;

    public PutEngine(Storage<Engine> engineStorage) {
        this.engineStorage = engineStorage;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(Factory.dynamicSettings.ENGINE_DEALER_TIME().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //System.out.println("Кладу в склад двигателей");
            engineStorage.put(new Engine((int) (Math.random() * 1000)));
        }
    }
}
